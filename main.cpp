#include <QtWidgets>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QWidget window;
    window.resize(300, 300);
    window.setWindowTitle("Hello World");
    window.show();

    return app.exec();
}
